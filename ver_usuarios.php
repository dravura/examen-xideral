<html>  
<head lang="en">  
    <meta charset="UTF-8">  
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">  
    <title>Usuarios</title>  
</head>  
<style>  
    .login-panel {  
        margin-top: 150px;  
    }  
    .table {  
        margin-top: 50px;  
  
    }  
  
</style>  
  
<body>  
  
<div class="table-scrol">  
    <h1 align="center">Todos los usuarios</h1>  
  
<div class="table-responsive"> 
  
    <table class="table table-bordered table-hover table-striped" style="table-layout: fixed">  
        <thead>  
  
        <tr>  
  
            <th>Id</th>  
            <th>Nombre</th>  
            <th>Correo</th>  
            <th>Pass</th>  
            <th>Borrar</th>  
        </tr>  
        </thead>  
  
        <?php  
        include("conexion.php");  
        $query="select * from usuarios"; 
        $result=mysqli_query($dbcon,$query);  
  
        while($row=mysqli_fetch_array($result)) 
        {  
            $user_id=$row[0];  
            $user_name=$row[1];  
            $user_email=$row[2];  
            $user_pass=$row[3];  
        ?>  
  
        <tr>  
<!--here showing results in the table -->  
            <td><?php echo $user_id;  ?></td>  
            <td><?php echo $user_name;  ?></td>  
            <td><?php echo $user_email;  ?></td>  
            <td><?php echo $user_pass;  ?></td>  
            <td><a href="borrar.php?del=<?php echo $user_id ?>"><button class="btn btn-danger">Borrar</button></a></td> 
        </tr>  
  
        <?php } ?>  
  
    </table>  
        </div>  
</div>  
  
  
</body>  
  
</html>